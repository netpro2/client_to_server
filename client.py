import socket

def run_client():
    host = 'localhost'
    port = 12345

    # สร้าง socket object
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # ลองเชื่อมต่อกับ server
    try:
        client_socket.connect((host, port))
    except ConnectionRefusedError:
        print("can't connect to server")
        return

    while True:
        # รับค่าที่ server ส่งมา
        data = client_socket.recv(1024).decode()
        if not data:
            break

        # แสดงค่าที่ได้รับทางหน้าจอ
        print("server => client", data)

        # คำนวณค่า +1 และส่งกลับไปยัง server
        number = int(data) + 1
        client_socket.send(str(number).encode())

    # ปิดการเชื่อมต่อ
    client_socket.close()

if __name__ == "__main__":
    run_client()
