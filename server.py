import socket

def run_server():
    host = 'localhost'
    port = 12345

    # สร้าง socket object
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # กำหนดค่าให้ socket ใช้งานได้กับ address และ port ที่ระบุ
    server_socket.bind((host, port))
    
    # กำหนดให้ socket เริ่มต้นรอการเชื่อมต่อจาก client
    server_socket.listen(1)
    print("connect to client...")

    # รับการเชื่อมต่อจาก client
    connection, client_address = server_socket.accept()
    print("connecting form: ", client_address)

    # ค่าเริ่มต้นของตัวเลข
    number = 1

    while number <= 100:
        # ส่งค่าตัวเลขไปยัง client
        connection.send(str(number).encode())

        # รับค่าที่ client ส่งกลับมา
        data = connection.recv(1024).decode()
        print("client => server", data)

        # นับตัวเลขขึ้นไป 1

        number = int(data)+1
        

    # ปิดการเชื่อมต่อ
    connection.close()
    server_socket.close()

if __name__ == "__main__":
    run_server()
